envresult = require('dotenv').config()
let express = require('express');
const app = express();
const router = express.Router();
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let http = require('http');
let path = require('path');
let async = require('async');
let mongo = require('mongodb');
let mongoose = require('mongoose');
let session = require('express-session');
let flash = require('express-flash');
let config = require('./app/config/config');
let dbconf = require('./app/config/db');
let cron = require('./app/config/cron');
const util = require('util'); // FOR DEBUGGING

if (envresult.error) {
  throw result.error
}

// View Engine
app.locals.basedir = path.join(__dirname, 'views');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Defining middleware
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

let sessionStore = new session.MemoryStore;

// Initializing req
app.use(function(req, res, next) {
  // Setting Admin ID
  req.admin = {};
  //req.admin.id = '5ea35d60f998cf7baaa71089'; // Admin
  req.admin.id = '6326441f5e25ca437e5628b9';  // Head Panda

  if(!res.uiErrors) res.uiErrors = [];
  if(!res.uiMessages) res.uiMessages = [];

  next();
});

app.use(session({
  secret: 'panda panda panda',
  resave: false,
  store: sessionStore,
  saveUninitialized: false,
  cookie: {secure: false} // Change to true for Prod
}));

// Enabling express-flash
app.use(flash());

// If there's a flash message in the session request, make it available in the response, then delete it
app.use(function(req, res, next){
  res.locals.sessionFlash = req.session.sessionFlash;
  delete req.session.sessionFlash;
  next();
});

// Pull messages from flash to expose to next route for redirects
app.all('*', function(req, res, next) {
  res.uiErrors = res.uiErrors.concat(req.flash('error'));
  res.uiMessages = res.uiMessages.concat(req.flash('info'));
  next();
});

// Custom functions for req
app.use(function (req, res, next) {
  req.buildError = function (status, message) {
    let cusErr = new Error(message);
    cusErr.status = status;
    return cusErr;
  };
  req.renderUI = function (res, view, data) {
    res.uiErrors = res.uiErrors.concat(req.flash('error'));
    res.uiMessages = res.uiMessages.concat(req.flash('info'));

    
    // Adding user data to response
    if(req.session.user) {
      if(!data.user) data.user = {};
      data.user.id = req.session.user.id;
      data.user.name = req.session.user.name,
      data.user.isAdmin = req.session.user.isAdmin,
      data.user.isGlobalModerator = req.session.user.isGlobalModerator
    }

    res.render(view, { 
      uiMessages: res.uiMessages,
      uiErrors: res.uiErrors,
      data: data
    });
  };

  next()
});

// todo: change from express-session's default server-side session storage 'memorystore' once in production
// npmjs.com/package/express-session

// Logger for all routes
app.use(function logger (req, res, next) {
  console.log(`METHOD:[${util.inspect(req.method)}]URL:[${util.inspect(req.url)}]BODY:[${JSON.stringify(req.body)}]`); // DEBUG
  //console.log(`HEADERS:[${util.inspect(req.headers)}]`); // DEBUG
  if (!req.session || !req.session.user) {
    if(req.url !== '/user/logout' && req.url !== '/user/login' && req.url !== '/user/create') {
      let errors = [];
      errors.push(req.buildError(401, 'You must be logged in first'));
      return req.renderUI(res, 'index', { data: { errors: errors } });
    }
  }
  
  next();
});

// Routes
let homeRouter = require('./routes/index');
let userRouter = require('./routes/user');
let roomRouter = require('./routes/room')
let privateRoomRouter = require('./routes/privateRoom')
let messageRouter = require('./routes/message')
let privateMessageRouter = require('./routes/privateMessage')

app.use('/', homeRouter);
app.use('/user/', userRouter);
app.use('/room/', roomRouter);
app.use('/privateRoom/', privateRoomRouter);
app.use('/message/', messageRouter);
app.use('/privateMessage/', privateMessageRouter);

app.listen(config.app.port, function () {
  console.log('Panda Chat has started listening on 3000');
});

app.use(function (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

//Handle Errors
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.send(err.message);
});

module.exports = app;
