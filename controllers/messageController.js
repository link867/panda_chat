const express = require('express');
const router = express.Router();
const async = require('async');
const mongoose = require('mongoose');
const { body, validationResult } = require('express-validator');
const Message = require('../models/message');
const Room = require('../models/room');
const User = require('../models/user');
const functions = require('../app/functions');
const util = require('util'); // FOR DEBUGGING

exports.send = [
  // Validate and sanitize contents
  body('contents', 'Message cannot be blank').isLength({ min: 1 }).trim(),
  body('contents').trim().escape(),

  // Process request
  function (req, res, next) {
    let errors = validationResult(req);
    if(!errors.isEmpty()) {
      for (err of errors.array()) {
        req.flash('error', err.msg);
      }
      return res.redirect('/room/view/' + req.params.room_id);
    }
    if (!req.params.room_id) return next(req.buildError(400, 'Missing parameter'));

    let message_contents = req.body.contents;
    Room
    .findById(req.params.room_id, function (err, room) {
      if (err) return next(err);
      if (!room) return next(req.buildError(404, 'Room does not exist'));
      if (room.status == 2) {
        let quotes = [
          "Party on dudes!",
          "Strange things are afoot at the Circle K",
          "Want a Twinkie, Genghis Khan?",
          "So, Bill, what you're telling me, essentially, is that Napoleon was a short dead dude?",
          "Dude, there's no way I can possibly do infinity push-ups!",
          "I'll eat you up like the warm, toasty little butter cakes you are",
          "Dinner's over, worm dude",
          "We have a destiny to fulfill. Think about our fans, dude!",
          "How's that stealing if we're stealing it from ourselves, dude?",
          "Dude! You were playing 40-minute bass solos. No one but you could play!",
          "One month ago, you played the Elks Lodge in Barstow, California, for forty people," + 
          "most of whom were there only because it was two-dollar taco night, whatever the hell that means",
          "I have a feeling things are about to change in a most outstanding way",
          "You might be a king or a little street sweeper, but sooner or later you dance with the reaper",
          "It's a history report, not a babe report",
          "Ted, you and I have witnessed many things, but nothing as bodacious as what just happened",
          "Gentlemen, we're history",
          "Most non-triumphant",
          "Billy, you are dealing with the oddity of time travel with the greatest of ease",
          "This is a dude who, 700 years ago, totally ravaged China, and who, we were told, 2 hours ago, totally ravaged Oshman's Sporting Goods",
          "You're not strong! You're silky boys! Silk comes from the butts of Chinese Worms!",
          "I believe Colonel Mustard did it in the study with the candlestick",
          "OK, we get it. You're a grateful, totally insecure, somehow dead robot named Dennis Caleb McCoy",
          "It seems to me the only thing you?ve learned is that Caesar is a salad dressing dude",
          "How's it going royal ugly dudes?",
          "So-crates. The only true wisdom consists in knowing that you know nothing"
        ];
        let num = Math.floor(Math.random() * (quotes.length));
        message_contents = quotes[num];
      }
      else if (room.status == 3) {
        req.flash('error', 'Messages cannot be sent in a deleted room');
        return res.redirect('/room/view/' + room._id);
      }

      let date = new Date();

      let newMessage = new Message({ 
        contents: message_contents,
        sender: req.body.senderID,
        toRoom: room._id,
        timezoneOffset: date.getTimezoneOffset()
      });

      newMessage.save(function (err) {
        if (err) return next(err);
        if (room.members.indexOf(req.body.senderID) == -1) {
          Room
          .updateOne(
            {_id: room._id}, 
            { 
              $push: { 
                messages: 
                newMessage, 
                members: req.body.senderID 
              } 
            }, function (err) {
              if(err) return next(err);
              return res.redirect('/room/view/' + room.id);
          });
        }
        else {
          Room
          .updateOne(
            {_id: room._id},
            {
              $push: {
                messages: newMessage
              }
            },
            function (err) {
            if(err) return next(err);
            return res.redirect('/room/view/' + room.id);
          });
        }
      });
    });
  }
];

exports.delete = function (req, res, next) {
  if (!req.params.message_id) return next(req.buildError(400, 'Missing parameter'));

  Message.findById(req.params.message_id, function (err, message) {
    if (err) return next(err);
    if (!message) return next(req.buildError(404, 'Message does not exist'));
    if (message.removed === 1 || message.deleted === 1) {
      req.flash('error', 'Message already removed or deleted');
      return res.redirect('/room/view/' + message.toRoom);
    }

    if (message.sender != req.session.user.id) {
      req.flash('error', 'Cannot delete another user\'s message');
      return res.redirect('/room/view/' + message.toRoom);
    }

    message.deleted = 1;

    message.save(function (err) {
      if (err) return next(err);
      return res.redirect('/room/view/' + message.toRoom);
    });
  });
}

exports.remove = function (req, res, next) {
  if (!req.params.message_id) return next(req.buildError(400, 'Missing parameter'));

  Message
  .findById(req.params.message_id)
  .populate('sender')
  .exec(function (err, message) {
    if (err) return next(err);
    if (!message) return next(req.buildError(404, 'Message does not exist'));
    if (message.removed === 1 || message.deleted === 1) {
      req.flash('error', 'Message already removed or deleted');
      return res.redirect('/room/view/' + message.toRoom);
    }

    User.findById(req.session.user.id, function (err, user) {
      if (err) return next(err);

      Room
      .findById(message.toRoom)
      .exec(function (err, room) {
        if (err) return next(err);

        let allowed = functions.canRemoveMessage(user, room, message);
        if (allowed === true) {
          message.removed = 1;
          message.save(function (err) {
            if (err) return next(err);
            return res.redirect('/room/view/' + message.toRoom);
          });
        } else {
          req.flash('error', 'Not permitted to remove');
          return res.redirect('/room/view/' + message.toRoom);
        }
      });
    });
  });
}
