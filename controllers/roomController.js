const express = require('express');
const router = express.Router();
const async = require('async');
const mongoose = require('mongoose');
const { body, validationResult } = require('express-validator');
const Room = require('../models/groupRoom');
const Message = require('../models/message');
const User = require('../models/user');
const functions = require('../app/functions');

exports.view = function(req, res, next) {
  if (!req.params.room_id) return next(req.buildError(400, 'Missing room ID'));

  Room
  .findById(req.params.room_id)
  .populate({
    path: 'messages',
    populate: {
      path: 'sender',
      model: 'User'
    }
  })
  .exec(function (err, room) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));

    User.findById (req.session.user.id).
    exec(function (err, user) {
      let is_room_banned = room.banned.indexOf(user._id) > -1;
      let filtered_messages = [];
      if (is_room_banned) {
        filtered_messages = [
          { 
            senderName: 'Admin', 
            senderID: req.admin.id,
            _id: mongoose.Types.ObjectId(), 
            contents: 'You are banned from viewing this room'
          }
        ];
      }
      else if(user.favor < room.favorMin) {
        filtered_messages = [
          { 
            senderName: 'Admin', 
            senderID: req.admin.id,
            _id: mongoose.Types.ObjectId(), 
            contents: 'Your favor is too low to view this room'
          }
        ];
      }
      else {
        room.messages.forEach(function(message) {
          let filtered_message = {};
          // Processs messages for displaying to user
          if (user.blocked.indexOf(message.sender._id) != -1) return;
          if (user.blockedBy.indexOf(message.sender._id) != -1) return;

          let can_remove = functions.canRemoveMessage(user, room, message);
          if (message.deleted === 1) {
            message.contents = '[deleted]';
            can_remove = false;
          } 
          else if (message.removed === 1) {
            message.contents = '[removed]';
            can_remove = false;
          }

          if (user.settings.safeMode && message.deleted !== 1 && mesage.removed !== 1) {
            // Word filter
            message.contents = message.contents.replace(/pepsi/gi, 'coke');
          }

          message.is_room_banned = room.banned.indexOf(message.sender._id) != -1;

          filtered_message = {
            _id: message._id,
            senderID: message.sender._id,
            senderName: message.sender.name,
            contents: message.contents,
            timestamp: message.timetamp,
            toRoom: message.toRoom,
            timezoneOffset: message.timezoneOffset,
            canRemove: can_remove,
            isRoomBanned: message.is_room_banned
          };
            
          filtered_messages.push(filtered_message);
        });
      } 

      let data = {
        room_id: req.params.room_id,
        name: room.name,
        messages: filtered_messages,
        members: room.members,
        moderators: room.moderators,
        owner: room.owner,
      };

      return req.renderUI(res, 'room/view', data);
    });
  });
}

exports.listAll = function(req, res, next) {
  Room
  .find()
  .where('status').equals(1)
  .exec(function (err, rooms) {
      if(err) return next(err);
      if(!rooms) return next(req.buildError(404, 'Failed to fetch room list'));

      let data = {
        rooms: rooms
      };

      return req.renderUI(res, 'room/listAll', data);
  });
}

exports.listUser = function(req, res, next) {
  Room
  .find({ members: req.session.user.id })
  .exec(function (err, rooms) {
      if(err) return next(err);
      if(!rooms) return next(req.buildError(404, 'Failed to fetch room list'));

      let data = {
        rooms: rooms
      };

      return req.renderUI(res, 'room/listUser', data);
  });
}

exports.createForm = function(req, res, next) {
  return req.renderUI(res, 'room/createForm', {});
};

exports.create = [
  // Validate and sanitize name
  body('name', 'Room Name cannot be blank').isLength({ min: 1 }).trim(),
  body('name').custom( function (name) {
    let re = /[n\s]+?[i1l\|!\s]+?[g\s]+(([e3\s]+?)?[r\s]+)?/ig;
    if (re.test(name)) {
      throw new Error('Vulgar words are not permitted in Room Name.');
    }
    return name
  }),
  body('name').trim().escape(),

  // Process request
  function (req, res, next) {
    let errors = validationResult(req);
    if(!errors.isEmpty()) {
      console.log('Errors in validation ' + JSON.stringify(errors)); // DEBUG

      errors.array().forEach(function(error) {
        res.uiErrors.push(error.msg);
      });

      let data = { 
        name: req.body.name
      };

      return req.renderUI(res, 'room/createForm', data);
    }

    let newRoom = new Room({
      name: req.body.name, 
      owner: req.session.user.id,
      members: [req.session.user.id],
      moderators: [req.session.user.id],
      isPrivate: req.body.isPrivate ? true : false,
    });

    newRoom.save().then(function (user, err) {
      if (err) return next(err);

      let date = new Date();
      let firstMessage = 'Welcome to the new room ' + newRoom.name + ".\n" +
                         'Remember Bill and Ted\'s mantra: "Be excellent to each other. ' + "\n" +
                         'And party on Dudes!"';

      let newMessage = new Message({ 
        contents: firstMessage,
        sender: req.admin.id,
        toRoom: newRoom._id,
        readStatus: false,
        timezoneOffset: date.getTimezoneOffset()
      });

      newMessage.save(function (err) {
        if (err) return next(err);
        Room
        .updateOne({ _id: newRoom._id}, { $push: { messages: newMessage } }, function (err) {
          if(err) return next(err);

          return res.redirect('/room/view/' + newRoom._id);
        });
      });
    });
  }
];

exports.join = function (req, res, next) {
  if(!req.body.room_id) return next(req.buildError(400, 'Missing room ID'));

  Room
  .findById(req.body.room_id)
  .exec(function (err, room) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));
    if (room.members.indexOf(req.session.user.id) > -1) return next(req.buildError(403, 'Already a member'));
    if (room.banned.indexOf(req.session.user.id) > -1) {
      req.flash('info', 'Cannot join a room that you are banned in');
      let redirect_url = '/room/view/' + req.body.room_id;
      return res.send({url: redirect_url});
    }
    if (room.status == 3) {
      req.flash('error', 'Cannot join a closed or deleted room');
      let redirect_url = '/room/view/' + req.body.room_id;
      return res.send({url: redirect_url});
    }
    

    User
    .findById(req.session.user.id)
    .exec(function (err, user) {
      if (err) return next(err);

      if(user.favor < room.favorMin) {
        req.flash('error', 'Favor is too low to join');
        let redirect_url = '/room/view/' + req.body.room_id;
        return res.send({url: redirect_url});
      }

      let date = new Date();
      let join_message_text = `${user.name} has joined the room`;
      let join_message = new Message({ 
        contents: join_message_text,
        sender: req.admin.id,
        toRoom: room._id,
        readStatus: false,
        timezoneOffset: date.getTimezoneOffset()
      });

      join_message.save(function (err, message) {
        if (err) return next(err);

        room.messages.push(join_message);
        room.members.push(req.session.user.id);

        room.save(function (err) {
          if(err) return next(err);

          let redirect_url = '/room/view/' + req.body.room_id;
          return res.send({url: redirect_url});
        });
      });
    });
  });
}

exports.leave = function (req, res, next) {
  if(!req.body.room_id) return next(req.buildError(400, 'Missing room ID'));
  
  Room
  .findById(req.body.room_id)
  .exec(function (err, room) {
    if(err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));
    if(room.members.indexOf(req.session.user.id) == -1) return next(req.buildError(403, 'Not a member'));

    User
    .findById(req.session.user.id)
    .exec(function (err, user) {
      if (err) return next(err);

      let date = new Date();
      let leave_message_text = `${user.name} has left the room`;
      let leave_message = new Message({ 
        contents: leave_message_text,
        sender: req.admin.id,
        toRoom: room._id,
        readStatus: false,
        timezoneOffset: date.getTimezoneOffset()
      });

      leave_message.save(function (err, message) {
        if (err) return next(err);
        room.messages.push(leave_message);
        room.members.pull(req.session.user.id);

        room.save(function (err) {
          if(err) return next(err);

          let redirect_url = '/room/list/user';
          return res.send({url: redirect_url});
        });
      })
    });
  });
}

exports.editForm = function (req, res, next) {
  if (!req.params.room_id) return next(req.buildError(400, 'Missing room ID'));

  Room
  .findById( req.params.room_id )
  .exec(function (err, room) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));
    if(req.session.user.id != room.owner) return next(req.buildError(403, 'Not permitted'));

    let data = { 
      room_id: room._id, 
      room_name: room.name, 
      is_private: room.isPrivate, 
      room_status: room.status,
      favor_min: room.favorMin
    } 

    return req.renderUI(res, 'room/editForm', data);
  });
}

exports.edit = [
  // Validate and sanitize name 
  body('name').trim().escape(),
  body('name', 'Room Name cannot be blank').isLength({ min: 1 }).trim(),
  body('name').custom( function (name) {
    let re = /[n\s]+?[i1l\|!\s]+?[g\s]+(([e3\s]+?)?[r\s]+)?/ig;
    if (re.test(name)) {
      throw new Error('Vulgar words are not permitted in Room Name.');
    }
    return name;
  }),
  body('favor_min').trim().escape(),
  body('favor_min', 'Minimum favor must be a number 0 or greater').isInt({ min: 0 }),

  // Process request
  function (req, res, next) {
    let errors = validationResult(req);
    if(!errors.isEmpty()) {
      errors.array().forEach(function(error) {
        req.flash('error', error.msg);
      });

      return res.redirect('/room/edit/' + req.body.room_id);
    }

    let roomIsPrivate = req.body.isPrivate ? true : false;

    Room
    .findById( req.body.room_id )
    .exec(function (err, room) {
      if (err) return next(err);
      if (!room) return next(req.buildError(404, 'Room does not exist'));
      if (req.session.user.id != room.owner) return next(req.buildError(403, 'Not permitted'));

      Room
      .updateOne({ _id: req.body.room_id}, { 
        name: req.body.name,
        isPrivate: roomIsPrivate,
        favorMin: req.body.favor_min
      }, function (err) {
        if (err) return next(err);
        return res.redirect('/room/view/' + room._id );
      });
    });
  }
];

exports.delete = function (req, res, next) {
  if (!req.body.room_id) return next(req.buildError(400, 'Missing room ID'));

  Room
  .findById(req.body.room_id)
  .exec(function (err, room) {
    if (err) return next(err);
    User
    .findById(req.session.user.id)
    .exec(function (err, user) {
      if (err) return next(err);
      if (!room) return next(req.buildError(404, 'Room does not exist'));
      if (!functions.canCloseOrDeleteRoom) return next(req.buildError(403, 'Not permitted'));
      if (room.status == 3) {
        req.flash('error', 'Room already deleted');
        let redirect_url = '/room/view/' + room._id;
        return res.send({url: redirect_url});
      }

      let date = new Date();
      let last_message_text = "This room has been deleted. No more messages can be sent.\n" +
                          'All we are is dust in the wind, dude.';
      let last_message = new Message({ 
        contents: last_message_text,
        sender: req.admin.id,
        toRoom: room._id,
        readStatus: false,
        timezoneOffset: date.getTimezoneOffset()
      });

      last_message.save(function (err, message) {
        if (err) return next(err);
        room.messages.push(last_message);
        room.status = 3;
        room.save(function (err) {
          req.flash('info', 'Room deleted');
          let redirect_url = '/room/view/' + room._id;
          return res.send({url: redirect_url});
        });
      });
    });
  });
}

exports.close = function (req, res, next) {
  if (!req.body.room_id) return next(req.buildError(400, 'Missing room ID'));

  Room
  .findById(req.body.room_id)
  .exec(function (err, room) {
    if (err) return next(err);
    User
    .findById(req.session.user.id) 
    .exec(function (err, user) {
      if (!room) return next(req.buildError(404, 'Room does not exist'));
      if (!functions.canCloseOrDeleteRoom) return next(req.buildError(403, 'Not permitted'));
      if (room.status == 2 || room.status == 3) {
        let redirect_url = '/room/view/' + room._id;
        let message = (room.status == 2) ? 'Room already closed' : 'Room already deleted';
        req.flash('error', message);
        return res.send({url: redirect_url});
      }

      let date = new Date();
      let close_message_text = "This room has been closed. Sending any more messages here would be most heinous.\n";
      let close_message = new Message({ 
        contents: close_message_text,
        sender: req.admin.id,
        toRoom: room._id,
        readStatus: false,
        timezoneOffset: date.getTimezoneOffset()
      });

      close_message.save(function (err, message) {
        if (err) return next(err);
        room.messages.push(close_message);
        room.status = 2;
        room.save(function (err) {
          req.flash('info', 'Room closed');
          let redirect_url = '/room/view/' + room._id;
          return res.send({url: redirect_url});
        });
      });
    });
  });
}

exports.modEdit = function (req, res, next) {
  if (!req.params.room_id) return next(req.buildError(400, 'Missing room ID'));

  Room.findById(req.params.room_id)
  .populate('members')
  .populate('moderators')
  .exec(function (err, room) {
    if (err) return next(err);

    let modIds = [];
    let validMods = []
    room.moderators.forEach(function(mod) {
      modIds.push(mod._id.toString());

      if (mod._id.toString() == room.owner._id.toString()) return;

      validMods.push({ id: mod._id, name: mod.name });
    });

    let canBeModded = [];
    room.members.forEach(function(member) {
      if (modIds.indexOf(member._id.toString()) != -1) return;
      if (member.isAdmin) return;
      if (member.isGlobalModerator) return;

      canBeModded.push({ id: member._id, name: member.name });
    });

    let data = { 
      room: {
        id: room._id, 
        name: room.name, 
        members: canBeModded, 
        mods: validMods, 
        owner: room.owner 
      }
    };

    return req.renderUI(res, 'room/modEditForm', data);
  });
}

exports.modAdd = function (req, res, next) {
  if (!req.params.room_id) return next(req.buildError(400, 'Missing room ID'));

  Room
  .findById( req.params.room_id )
  .exec(function (err, room) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));

    User.findById( req.session.user.id, function (err, user) {
      if (err) return next(err);

      let canAdd = false;
      if (user._id.str == room.owner.str) {
        canAdd = true;
      } else if (user.isAdmin) {
        canAdd = true;
      } else if (user.isGlobalModerator) {
        canAdd = true;
      }

      if(!canAdd) return next(req.buildError(403, 'Not permitted'));

      let addIds = [];
      for (let bodyItem in req.body) {
        let ObjectId = require('mongoose').Types.ObjectId
        if (ObjectId.isValid(bodyItem)) {
          addIds.push(bodyItem);
        }
      }

      let cursor = User.find().where('_id').in(addIds).select('_id').cursor();
      cursor.on('data', function (user) {
        if (room.moderators.indexOf(user._id) == -1) {
          room.moderators.push(user._id);
        }
      });
      cursor.on('close', function () {
        room.save(function (err) {
          if (err) return next(err);
          return res.redirect('/room/view/' + req.params.room_id);
        });
      });
    });
  });
}

exports.modRemove = function (req, res, next) {
  if (!req.params.room_id) return next(req.buildError(400, 'Missing room ID'));

  Room
  .findById(req.params.room_id)
  .exec(function (err, room) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));

    User
    .findById(req.session.user.id, function (err, user) {
      if (err) return next(err);

      let canRemove = false;
      if (user._id.str == room.owner.str) {
        canRemove = true;
      } else if (user.isAdmin) {
        canRemove = true;
      } else if (user.isGlobalModerator) {
        canRemove = true;
      }

      if(!canRemove) return next(req.buildError(403, 'Not permitted'));

      let addIds = [];
      for (let bodyItem in req.body) {
        let ObjectId = require('mongoose').Types.ObjectId
        if (ObjectId.isValid(bodyItem)) {
          addIds.push(bodyItem);
        }
      }

      let cursor = User.find().where('_id').in(addIds).select('_id').cursor();
      cursor.on('data', function (user) {
        if (room.moderators.indexOf(user._id) != -1) {
          room.moderators.pull(user._id);
        }
      });
      cursor.on('close', function () {
        room.save(function (err) {
          if (err) return next(err);
          return res.redirect('/room/view/' + req.params.room_id);
        });
      });
    });
  });
}

exports.banUser = function (req, res, next) {
  if (!req.params.banee_id || !req.params.room_id) return next(req.buildError(400, 'Missing parameter'));

  Room
  .findById(req.params.room_id, function (err, room) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));

    User.findById(req.session.user.id, function (err, banner) {
      if (err) return next(err);
      User.findById(req.params.banee_id, function (err, banee) {
        if (err) return next(err);
        if (!functions.canUserBeBanned(banner, banee, room)) {
          req.flash('info', 'Not permitted to ban');
          return res.redirect('/room/view/' + req.params.room_id);
        }

        if (room.banned.indexOf(req.params.banee_id.toString()) === -1) {
          room.banned.push(req.params.banee_id);
        } 
        else {
          req.flash('info', 'Already banned');
          return res.redirect('/room/view/' + req.params.room_id);
        }

        room.save(function (err) {
          if (err) return next(err);

          let date = new Date();
          banee.lastBanned = date.toUTCString(); 

          banee.save(function (err, savedBanee) {
            req.flash('info', 'User banned');
            return res.redirect('/room/view/' + req.params.room_id);
          });
        });
      });
    });
  });
}

exports.unBanUser = function (req, res, next) {
  if (!req.params.banee_id || !req.params.room_id) return next(req.buildError(400, 'Missing parameter'));

  Room.findById(req.params.room_id, function (err, room) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));

    User.findById(req.session.user.id, function (err, banner) {
      if (err) return next(err);
      User.findById(req.params.banee_id, function (err, banee) {
        if (err) return next(err);
        if (!functions.canUserBeBanned(banner, banee, room)) {
          req.flash('info', 'Not permitted to unban');
          return res.redirect('/room/view/' + req.params.room_id);
        }

        let banned_index = room.banned.indexOf(mongoose.Types.ObjectId(banee._id));
        if (banned_index === -1) {
          req.flash('info', 'User not banned');
          return res.redirect('/room/view/' + req.params.room_id);

        }

        room.banned.pull(room.banned[banned_index]);
        room.save(function(err) {
          if (err) return next(err);
          req.flash('info', 'User unbanned');
          return res.redirect('/room/view/' + req.params.room_id);
        });
      });
    });
  });
}

exports.listBannedUsers = function (req, res, next) {
  if (!req.params.room_id) return next(req.buildError(400, 'Missing parameter'));

  Room
  .findById( req.params.room_id )
  .populate('banned')
  .exec(function (err, room) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));
    if(req.session.user.id != room.owner) return next(req.buildError(403, 'Not permitted'));

    let banned_users = [];
    room.banned.forEach(function(banee) {
      banned_users.push({
        name: banee.name,
        id: banee._id,
        reason: banee.reason,
        timeBanned: banee.timeBanned,
        timeUnbanned: banee.timeUnbanned
      });
    });

    banned_users.sort(function(a, b){
      return a.timeUnbanned > b.timeUnbanned;
    });

    let data = {
      room_id: room._id,
      banned_users: banned_users,
      errors: null
    }

    return req.renderUI(res, 'room/listBannedUsers', data);
  });
}
