const express = require('express');
const router = express.Router();
const async = require('async');
const mongoose = require('mongoose');
const { body, validationResult } = require('express-validator');
const User = require('../models/user');
const Report = require('../models/report');
const Message = require('../models/message');
let policyStr = 'Password must be at least 10 characters long with at least one uppercase letter, one number, and one special character.';
const util = require('util'); // DEBUG

exports.login = [ 
  body('email').trim().escape(),
  body('email', 'Must be a valid email').isEmail(),
  body('password', 'Please enter a password').isLength({ min: 1}).trim(),
  function (req, res, next) {
    let errors = validationResult(req);
    if(!errors.isEmpty()) {
      console.log('Errors in validation ' + JSON.stringify(errors.array())); // DEBUG //
      let data = {
        email: req.body.email
      };
      for (err of errors.array()) {
        req.flash('error', err.msg);
      }

      return req.renderUI(res, 'index', data);
    }

    let favor = require('../app/favor.js'); // TESTING
    favor.iterateMessages(); // TESTING

    User.authenticate(req.body.email, req.body.password, function (err, user) {
      if (err) {
        req.flash('error', err);
        let data = {
          email: req.body.email
        };

        return req.renderUI(res, 'index', data);
      };

      req.session.user = { 
        name: user.name, 
        id: user._id, 
        isAdmin: user.isAdmin,
        isGlobalModerator: user.isGlobalModerator
      };

      uiData = { 
        user: {
          id: req.session.user.id,
          name: req.session.user.name,
          isAdmin: user.isAdmin,
          isGlobalModerator: user.isGlobalModerator
        }
      };

      return req.renderUI(res, 'index', uiData); 
    });
  }
]

exports.logout = function(req, res, next) {
  if (req.session) {
    console.log('@@ ' + JSON.stringify(req.session) + ' @@');
    // TODO: Look into the security implications of this
    req.session.regenerate(function(err) {
      if (err) return next(err);
      console.log('@@ ' + JSON.stringify(req.session) + ' @@');
      return req.renderUI(res, 'index', {}); 
    });
  } 
}

exports.profileShow = function(req, res, next) {
  const uid = req.params.user_id && req.params.user_id !== req.session.user.id ? req.params.user_id : req.session.user.id;

  User.findById(uid, function (err, user) {
    if (err) return next(err);
    if (!user) return next(req.buildError(404, 'User does not exist'));

    let isBlocked = false;
    let isSelf = true;
    let hasBlocked = false;
    let isFriend = false;
    let friends = [];
    if (req.params.user_id && req.params.user_id !== req.session.user.id) {
      isSelf = false;
      if (user.blocked.indexOf(req.session.user.id) !== -1) {
        isBlocked = true;
      }
      if (user.blockedBy.indexOf(req.session.user.id) !== -1) {
        hasBlocked = true;
      }
      if (user.friendedBy.indexOf(req.session.user.id) !== -1) {
        isFriend = true;
      }
    }
    else {
      friends = user.friends;
    }

    let userData = {};
    if (isBlocked) {
      userData = {
        otherUser: {
          id: user._id,
          isBlocked: isBlocked,
         }	
      };
    }
    else {
      if(isSelf) {
        userData = {
          otherUser: { 
            id: user._id,
            name: user.name,
            age: user.age,
            friends: friends,
            isBlocked: isBlocked,
            hasBlocked: hasBlocked,
            isSelf: isSelf,
            isFriend: isFriend,
            favor: user.favor
          }
        }
      }
      else {
        userData = {
          otherUser: { 
            id: user._id,
            name: user.name,
            age: user.age,
            isBlocked: isBlocked,
            hasBlocked: hasBlocked,
            isSelf: isSelf,
            isFriend: isFriend,
          }
        }
      }
    }

    return req.renderUI(res, 'user/profileShow', userData);
  });
};

exports.createForm = function(req, res) {
  return req.renderUI(res, 'user/createForm', {});
};

function validateAge(age) {
  if (isNaN(age)) {
    throw new Error('Must be a valid age');
  }
  else if (age < 18) {
    throw new Error('You must be 18+ to use this app');
  }
  else if (age > 122) {
    throw new Error('Call Ripley\'s, because you have broken a record \;\)');
  }
  else if(age == 69) {
    throw new Error('Nice.');
  }
}

exports.create = [
  body('name', 'Display Name cannot be blank').isLength({ min: 1 }).trim(),
  body('name').custom( function (name) {
    let re = /[n\s]+?[i1l\|!\s]+?[g\s]+(([e3\s]+?)?[r\s]+)?/ig;
    if (re.test(name)) {
      throw new Error('Vulgar words are not permitted in Display Name.');
    }
    return name
  }),
  body('name').trim().escape(),
  body('email').trim().escape(),
  body('email', 'Must be a valid email').isEmail(),
  body('email').custom( function (email) {
    let checkForDupeEmail = new Promise( function (resolve, reject) {
      return User.findOne({ 'email': email }, function (err, qres, callback) {
        if (err) reject(err);
        if (qres) {
          return reject('Email already in use');
        }
        return resolve();
      });
    });

    checkForDupeEmail.then( function() {} ).catch( function (reason) {
      return new Error(reason);
    });
    return checkForDupeEmail;
  }),
  body('password', policyStr)
    .isLength({ min: 10}).trim()
    .matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^\w\d]).*/)
    .custom( function confirmPasswordsMatch (pass, { req } ) {
      //if(pass !== req.body.passwordConf) {
      //  throw new Error('Passwords do not match');
      //} // UNCOMMENT AFTER DONE TESTING AND ADD CONFIRMATION FIELD IN FORM
      return true;
    }),
  body('age', 'Must be a valid age')
  .exists()
  .custom( function (age) {
    validateAge(age);
    return true; // Used to circumvent an expreses-validator bug
  }), function (req, res, next) {
    console.log('REQ: ' + JSON.stringify(req.body)); // DEBUG
    let errors = validationResult(req);
    if(!errors.isEmpty()) {
      // TODO: Figure out how to remove duplicate errors
      let message;
      for (let error of errors.array()) {
        if ( error.msg === message) {
          delete errors.array().error
          continue
        }
        message = error.msg;
      }

      let data = {
        email: req.body.email, 
        name: req.body.name,
        age: req.body.age,
        settings: {
          safeMode: req.body.safeMode,
          restritcPms: req.body.restrictPms
        }
      };
      for (err of errors.array()) {
        res.uiErrors.push(err.msg);
      }

      return req.renderUI(res, 'user/createForm', data);
    }

    let newUser = new User({
      name: req.body.name, 
      email: req.body.email,
      age: req.body.age,
      password: req.body.password,
      passwordConf: req.body.password,
      isGlobalModerator: false,
      isAdmin: false,
      safeMode: true
    });

    newUser.save(function (err, user) {
      if (err) {
        let data = {
          email: req.body.email, 
          name: req.body.name, 
          errors: err 
        };
        res.uiErrors.push(err);

        return next(req.buildError(res, 'user/createForm', data));
      }

      User.authenticate(user.email, user.password, function (err, user) {
        if (err) return next(err);
        if (!user) return next(req.buildError(500, 'Failed to create user'));

        req.session.user = { 
          name: user.name, 
          id: user._id 
        };

        req.renderUI(res, 'index', {});
      });
    });
  }
];

exports.profileUpdateForm = function(req, res, next) {
  User.findById( req.session.user.id, function (err, user) {
    if (err) return next(err);
    if (!user) return next(req.buildError(404, 'User not found'));

    let data = {
      user: { 
        email: user.email, 
        age: user.age,
      } 
    };

    return req.renderUI(res, 'user/updateForm', data);
  });
};

exports.profileUpdate =  [
  body('email').trim().escape(),
  body('email', 'Must be a valid email')
  .exists()
  .isEmail()
  .custom( function (email, { req }) { 
    let checkForDupeEmail = new Promise( function (resolve, reject) {
      return User.findOne({ email: email }, function (err, user, callback) {
        if (err) return reject(err);
        if (!user) return resolve();
        if (user.email && user._id != req.session.user.id ) {
          return reject('Email already in use');
        }
        return resolve();
      });
    });

    checkForDupeEmail.then( function() {} ).catch( function (reason) {
      return new Error(reason);
    });
    return checkForDupeEmail;
  }),

  body('age', 'Must be a valid age')
  .exists()
  .custom( function (age) {
    validateAge(age);
    return true; // Used to circumvent an expreses-validator bug
  }),
  
  function (req, res, next) {
    let errors = validationResult(req);

    if(!errors.isEmpty()) {
      console.log('Errors in validation ' + JSON.stringify(errors.array())); // DEBUG
      for (err of errors.array()) {
        res.uiErrors.push(err.msg);
      }

      let data = {
        email: req.body.email, 
        name: req.body.name,
        age: req.body.age,
      };

      return req.renderUI(res, 'user/updateForm', data);
    }

    User.findById(req.session.user.id, function (err, user) {
      if (err) return next(err);
      if (!user) return next(req.buildError(404, 'User not found'));
      if (user._id != req.session.user.id) return next(req.buildError('Not permitted', 403));

      user.name = req.body.name;
      user.email = req.body.email;
      user.age = req.body.age;
      user.favor = req.body.favor;

      // user.isAdmin = true; // DEBUG

      console.log(JSON.stringify(user));
      user.save(function (err, updatedUser) {
        if (err) return next(err)

        return req.redirect('user/profileShow');
      });
    });
  }
];

exports.profileDelete = function (req, res, next) {
  User.findById(req.session.user.id, function (err, user) {
    if (err) return next(err);
    if (!user) return next(req.buildError(404, 'User not found'));
    if (req.session.user.id != user._id) return next(req.buildError(403, 'Not permitted'));
    
    User.findByIdAndDelete(req.session.user.id, function(err) {
      if (err) next(err);
      res.redirect('/user/logout');
    });
  });
};

exports.passwordChangeForm = function (req, res, next) {
  User.findById(req.session.user.id, function(err, user) {
    if(err) next(err);
    if (!user) return next(req.buildError(404, 'User not found'));

    req.renderUI(res, 'user/passwordChangeForm', {});
  });
}

exports.passwordChange = [
  // Validate password is within policy constraints
  body('newPassword', policyStr)
    .isLength({ min: 10}).trim()
    .matches(/[A-Z0-9\$\#\@\!\%\^\&\*\(\)\-\_\+\=\~\|\{\}\:\;\'\"\>\<\.\,\]\[\\\/]/)
    .custom( function (pass, { req } ) {
      if(pass !== req.body.newPasswordConf) {
        throw new Error('Passwords do not match');
      }
      return true;
    }),
  function (req, res, next) {
    let errors = validationResult(req);
    if(!errors.isEmpty()) {
      for (err of errors.array()) {
        res.uiErrors.push(err.msg);
      }

      return req.renderUI(res, 'user/passwordChangeForm', {}); 
    }

    User.findById(req.session.user.id, function(err, user) {
      if(err) next(err);
      if (!user) return next(req.buildError(404, 'User not found'));
      if(user.password === req.body.newPassword) {
        res.uiErrors.push('Cannot use current password');

        return req.renderUI(res, 'user/passwordChangeForm', {});
      }
      

      // TODO: hash this before save
      //user.password = req.body.newPassword;
      //user.passwordConf = req.body.newPasswordConf

      user.save(function(err) {
        if (err) return next(err);

        return res.redirect('/user/profile');
      });
    });
  }
];

exports.settingsUpdateForm = function (req, res, next) {
  User.findById( req.session.user.id, function (err, user) {
    if (err) return next(err);
    if (!user) return next(req.buildError(404, 'User not found'));

    let data = {
      user: { 
        email: user.email,
        age: user.age,
        safeMode: user.settings.safeMode,
        restrictPms: user.settings.restrictPms,
        favorMin: user.settings.favorMin
      }
    };

    return req.renderUI(res, 'user/settingsForm', data);
  });
};

exports.settingsUpdate = [
  body('favor_min').trim().escape(),
  body('favor_min', 'Minimum favor must be a number greater than 0').isInt({ min: 0 }),

  function (req, res, next) {
    let errors = validationResult(req);
    if(!errors.isEmpty()) {
      for (err of errors.array()) {
        res.uiErrors.push(err.msg);
      }

      return req.renderUI(res, 'user/settingsForm', {}); 
    }

    User.findById(req.session.user.id, function (err, user) {
      if (err) return next(err);
      if(!user) return next(req.buildError(404, 'User not found'));

      user.settings.safeMode = (req.body.safeMode) ? true : false;
      user.settings.restrictPms = (req.body.restrictPms) ? true : false;
      user.settings.favorMin = req.body.favor_min;
      user.save(function (err, updatedUser) {
        if (err) return next(err);

        let data = {
          user: { 
            email: user.email,
            age: user.age,
            safeMode: updatedUser.settings.safeMode,
            restrictPms: updatedUser.settings.restrictPms,
            favorMin: updatedUser.settings.favorMin
          }
        };

        req.flash('info', 'Settings updated');
        return req.renderUI(res, 'user/settingsForm', data);
      });
    });
  }
]

exports.friendList = function (req, res, next) {
  User
  .findById(req.session.user.id)
  .populate('friends')
  .exec(function (err, user) {
    if (err) return next(err);
    if (!user) return next(req.buildError(404, 'User not found'));

    let friends = [];
    user.friends.forEach(function(friend) {
      friends.push({
        name: friend.name,
        id: friend._id
      });
    }); 
    let data = {
      friends: friends
    };

    return req.renderUI(res, 'user/friendList', data);
  });
};

exports.friendAdd = function (req, res, next) {
  User.findById(req.session.user.id, function(err, user) {
    if (err) return next(err);
    if (!user) return next(req.buildError(404, 'User not found'));
    if (user.friends.indexOf(req.params.user_id) !== -1) {
      return res.redirect('/user/profile/' + req.params.user_id);
    } 
    else if (user.blocked.indexOf(req.params.user_id) !== -1) {
      return next(req.buildError(403, 'Cannot add a blocked user to friends'));
    }
 
    User.findById(req.params.user_id, function(err, friendee) {
      if (err) return next(err);
      if (!friendee) return next(req.buildError(404, 'User not found'));

      user.friends.push(friendee._id);
      user.save(function (err) {
        if (err) return next(err);
        friendee.friendedBy.push(user._id);
        friendee.save(function (err) {
          console.log(`Added friend ${friendee._id}`);
          return res.redirect('/user/profile/' + friendee._id);
        });
      });
    });
  });
};

exports.friendRemove = function (req, res, next) {
  User.findById(req.session.user.id, function (err, user) {
    if (err) return next(err);
    if (!user) return next(req.buildError(404, 'User not found'));
    if (user.friends.indexOf(req.params.user_id) === -1) {
      return res.redirect('/user/profile/' + req.params.user_id);
    }

    User.findById(req.params.user_id, function (err, friendee) {
      if (err) return next(err);
      if (!friendee) return next(req.buildError(404, 'User not found'));
      user.friends.splice(user.friends.indexOf(req.params.user_id), 1);
      friendee.friendedBy.splice(friendee.friendedBy.indexOf(user._id), 1);
      user.save(function (err) {
        if (err) return next(err);
        friendee.save(function (err) {
          if (err) return next(err);
          return res.redirect('/user/profile/' + req.params.user_id);
        });
      });
    });
  });
};

exports.viewBlocked = function (req, res, next) {
  User
  .findById(req.session.user.id)
  .populate('blocked')
  .exec(function (err, user) {
    if (err) return next(err);
    if (!user) return next(req.buildError(404, 'User not found'));

    let blocked_data = [];
    user.blocked.forEach(function(blockee) {
      blocked_data.push({id: blockee.id, name: blockee.name});
    });

    let data = {
      user: { 
        blocked: blocked_data
      } 
    };

    return req.renderUI(res, 'user/viewBlocked', data);
  });
};

exports.block = function (req, res, next) {
  if(!req.params.blockee) return next(req.buildError(400, 'Missing blockee parameter'));
  if(!req.params.blockee) console.log('Missing blockee parameter'); // DEBUG

  const blockee_id = req.params.blockee;
  const blocker_id = req.session.user.id;

  User.findById( blockee_id, function (err, blockee) {
    if (err) return next(err);
    if (!blockee) console.log('User not found'); // DEBUG
    if (!blockee) return next(req.buildError(404, 'User not found'));
    if (blockee.blockedBy.indexOf(blocker_id) !== -1) console.log('Already blocked'); // DEBUG
    if (blockee.blockedBy.indexOf(blocker_id) !== -1) return next(req.buildError(403, 'Already blocked'));
    if (blockee.isAdmin) {
      req.flash('error', 'Admin cannot be blocked');
      return res.redirect('/user/profile/' + blockee.id);
    }

    User.findById( blocker_id, function (err, blocker) {
      if (err) return next(err);
      if (!blocker) return next(req.buildError(404, 'User not found'));

      if(blockee.blockedBy.indexOf(blocker_id) === -1) {
        blockee.blockedBy.push(blocker_id);
      }

      if(blocker.blocked.indexOf(blockee_id) === -1) {
        blocker.blocked.push(blockee_id);
      }

      let date = new Date();
      blockee.lastBlocked = date.toUTCString(); 

      blockee.save(function (err, updatedBlockee) {
        if (err) return next(err)
        blocker.save(function (err, updatedBlocker) {
          if (err) return next(err)
          return res.redirect('/user/profile/' + blockee.id);
        });
      });
    });
  });
}

exports.unBlock = function (req, res, next) {
  if(!req.params.blockee) return next(req.buildError(400, 'Missing blockee parameter'));

  const blockee_id = req.params.blockee;
  const blocker_id = req.session.user.id;

  User.findById( blockee_id, function (err, blockee) {
    if (err) return next(err);
    if (!blockee) return next(req.buildError(404, 'User not found'));
    if(blockee.blockedBy.indexOf(blocker_id) === -1) return next(req.buildError(403, 'Not blocked'));

    User.findById( blocker_id, function (err, blocker) {
      if (err) return next(err);
      if (!blocker) return next(req.buildError(404, 'User not found'));

      if(blockee.blockedBy.indexOf(blocker_id) !== -1) {
        blockee.blockedBy.splice(blockee.blockedBy.indexOf(blocker_id), 1);
      }

      if(blocker.blocked.indexOf(blockee_id) !== -1) {
        blocker.blocked.splice(blocker.blocked.indexOf(blockee_id), 1);
      }

      blockee.save(function (err, updatedBlockee) {
        if (err) return next(err);
        blocker.save(function (err, updatedBlocker) {
          if (err) return next(err)
          return res.redirect('/user/profile/' + blockee.id);
        });
      });
    });
  });
}

exports.reportMessageForm = function (req, res, next) {
  if(!req.params.reportee_id || !req.params.message_id || !req.params.room_id) return next(req.buildError(400, 'Missing required parameter'));
  if(req.params.reportee_id == req.session.user.id) {
    req.flash('error', 'Cannot self report');
    return res.redirect('/room/view/' + req.params.room_id);
  }
  User.findById(req.params.reportee_id, function (err, reportee) {
    if(err) return next(err);
    if(!reportee) return next(req.buildError(404, 'User not found'));

    Message.findById(req.params.message_id, function (err, message) {
      if(err) return next(err);
      if(!message) return next(req.buildError(404, 'Message not found'));
      let data = {
        reportee: {
          id: reportee._id,
          name: reportee.name
        },
        message: {
          id: message._id,
          contents: message.contents
        },
        room_id: req.params.room_id
      };

      return req.renderUI(res, 'user/reportMessageForm', data);
    });
  });
}

exports.reportMessage = function (req, res, next) {
  if(!req.body.reportee_id || (!req.body.reason && !req.body.message_id) || !req.body.room_id) return next(req.buildError(400, 'Missing required parameter'));
  if(req.body.reportee_id == req.session.user.id) return next(req.buildError(404, 'Cannot report self'));

  let reportee_id = req.body.reportee_id;
  let reporter_id = req.session.user.id;
  let message_id = req.body.message_id ? req.body.message_id : '';
  let reason = req.body.reason ? req.body.reason : '';
  let room_id = req.body.room_id ? req.body.room_id : '';
  
  User.findById(reportee_id, function (err, reportee) {
    if(err) return next(err);
    if(!reportee) return next(req.buildError(404, 'User not found'));
    let report = new Report({
      reporter: reporter_id,
      reportee: reportee._id,
      reason: reason,
      message: message_id
    });
    report.save(function (err, savedReport) {
      if(err) return next(err);
      let date = new Date();
      reportee.lastReported = date.toUTCString(); 
      reportee.save(function (err, savedUser) {
        req.flash('info', 'Report submitted');
        return res.redirect('/room/view/' + room_id);
      });
    });
  });
};

