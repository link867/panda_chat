const express = require('express');
const router = express.Router();
const async = require('async');
const mongoose = require('mongoose');
const { body, validationResult } = require('express-validator');
let Message = require('../models/message');
let PrivateRoom = require('../models/privateRoom');
let User = require('../models/user');


exports.send = [
  // Validate and sanitize contents
  body('contents', 'Message cannot be blank').isLength({ min: 1 }).trim(),
  body('senderID', 'Missing sender ID').isLength({ min: 1 }).trim(),
  body('contents').trim().escape(),

  // Process request
  function (req, res, next) {
    let errors = validationResult(req);
    if(!errors.isEmpty()) return res.redirect('/privateRoom/view/' + req.params.room_id);

    PrivateRoom
    .findById(req.params.room_id)
    .populate('members')
    .populate('pastMembers')
    .exec(function (err, room) {
      if (err) return next(err);
      if (!room) return next(req.buildError(404, 'Room does not exist'));

      let memberList = [];
      if(room.members.length > 1) {
        memberList = room.members;
      }
      else {
        memberList = room.pastMembers;
      }

      let user = {};
      let otherUser = {};
      if(memberList[0]._id.toString() === req.session.user.id.toString()) {
        user = memberList[0];
        otherUser = memberList[1];
      }
      else {
        user = memberList[1];
        otherUser = memberList[0];
      }
      console.log(`user: ${JSON.stringify(user)}\notherUser: ${JSON.stringify(otherUser)}`);
      if (user && user.blocked.includes(otherUser._id)) {
        req.flash('error', 'Cannot message a blocked user');
        return res.redirect('/privateRoom/view/' + room._id);
      }
      else if (otherUser && otherUser.blocked.includes(user._id)) {
        req.flash('error', 'You have been blocked by this user');
        return res.redirect('/privateRoom/view/' + room._id);
      }

      let date = new Date();
      let newMessage = new Message({ 
        sender: req.session.user.id,
        contents: req.body.contents,
        toRoom: room._id,
        timezoneOffset: date.getTimezoneOffset()
      });

      room.addMessage(newMessage, function(err) {
        if (err) return next(err);
        return res.redirect('/privateRoom/view/' + room._id);
      });
    });
  }
];

exports.delete = function (req, res, next) {
  if (!req.params.message_id) return next(req.buildError(400, 'Missing parameter'));

  Message
  .findById(req.params.message_id)
  .populate('sender')
  .exec(function (err, message) {
    if (err) return next(err);
    if (!message) return next(req.buildError(404, 'Message does not exist'));
    if (message.removed === 1 || message.deleted === 1) {
      req.flash('error', 'Message already removed or deleted');
      return res.redirect('/privateRoom/view/' + message.toRoom);
    }
    if (message.sender._id != req.session.user.id) {
      req.flash('error', 'Cannot delete another user\'s messages');
      return res.redirect('/privateRoom/view/' + message.toRoom);
    }

    message.deleted = 1;

    message.save(function (err) {
      if (err) return next(err);
      return res.redirect('/privateRoom/view/' + message.toRoom);
    });
  });
}

exports.remove = function (req, res, next) {
  if (!req.params.message_id) return next(req.buildError(400, 'Missing parameter'));

  Message.findById( req.params.message_id, function (err, message) {
    if (err) return next(err);
    if (!message) return next(req.buildError(404, 'Message does not exist'));
    if (message.removed === 1 || message.deleted === 1) {
      req.flash('error', 'Message already removed or deleted');
      return res.redirect('/privateRoom/view/' + message.toRoom);
    }

    User.findById( req.session.user.id, function (err, user) {
      if (err) return next(err);

      PrivateRoom.findById( message.toRoom, function (err, room) {
        if (err) return next(err);

        let allowed = false;
        if (user.isAdmin) {
          allowed = true;
        } else if (user.isGlobalModerator) {
          allowed = true;
        } else if (room.moderators.indexOf(user._id) !== -1) {
          allowed = true;
        }

        if (allowed === true) {
          message.removed  = 1;
          message.save(function (err) {
            if (err) return next(err);
            return res.redirect('/privateRoom/view/' + message.toRoom);
          });
        } else {
          req.flash('error', 'Not permitted to remove');
          return res.redirect('/privateRoom/view/' + message.toRoom);
        }
      });
    });
  });
}
