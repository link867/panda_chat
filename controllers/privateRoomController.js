const express = require('express');
const router = express.Router();
const async = require('async');
const mongoose = require('mongoose');
const { body, validationResult } = require('express-validator');
let PrivateRoom = require('../models/privateRoom');
let Message = require('../models/message');
let User = require('../models/user');

let createPrivateRoom = function (req, user, initiatingUser, callback) {
  let newRoom = new PrivateRoom({
    members: [
      user._id,
      initiatingUser._id
    ],
    pastMembers: [
      user._id,
      initiatingUser._id
    ],
    allowPictures: true,
  });

  newRoom.save().then(function (room, err) {
    if (err) return callback(err)
    callback(err, '/privateRoom/view/' + room._id);
  });
}

exports.open = function(req, res, next) {
  if (!req.body.user_id) return next(req.buildError(400, 'Missing needed info'));

  PrivateRoom
  .findOne({ 'members': { $all: [mongoose.Types.ObjectId(req.body.user_id), mongoose.Types.ObjectId(req.session.user.id)] } })
  .exec(function (err, room, callback) {
    if (err) return next(err);
    if (room) {
      let room_url = '/privateRoom/view/' + room._id;
      return res.send({url: room_url});
    }

    User.findById(req.body.user_id, function(err, user) {
      if (err) return next(err);
      User.findById(req.session.user.id, function(err, initiatingUser) {
        if (err) return next(err);
        
        let redirect_url = '/user/profile/' + user._id
        if (initiatingUser.blocked.includes(user._id)) {
          req.flash('error', 'You have this user blocked');
          return res.send({url: redirect_url});
        } 
        else if (user.blocked.includes(initiatingUser._id)) {
          req.flash('error', 'You have been blocked by this user');
          return res.send({url: redirect_url});
        }
        else if (initiatingUser.favor < user.settings.favorMin) {
          req.flash('error', 'You do not have enough favor to message this user');
          return res.send({url: redirect_url});
        }

        createPrivateRoom(req, user, initiatingUser, function (err, url) {
          if (err) return next(err)
          return res.send({url: url});
        });
      });
    });
  });
}

exports.view = function(req, res, next) {
  if (!req.params.room_id) return next(req.buildError(400, 'Missing needed info'));

  PrivateRoom
  .findById(req.params.room_id)
  .populate({
    path: 'messages',
    populate: {
      path: 'sender',
      model: 'User'
    }
   })
  .populate('members')
  .populate('pastMembers')
  .exec(function (err, room, callback) {
    if (err) return next(err);
    if (!room) return next(req.buildError(404, 'Room does not exist'));
    if (room.members[0]._id != req.session.user.id && room.members[1]._id != req.session.user.id) {
      return next(req.buildError(403, 'You are not a member of this room'));
    }

    let memberList = [];
    if(room.members.length > 1) {
      memberList = room.members;
    }
    else {
      memberList = room.pastMembers;
    }

    let user = {};
    let otherUser = {};
    if(memberList[0]._id.toString() === req.session.user.id.toString()) {
      user = memberList[0];
      otherUser = memberList[1];
    }
    else {
      user = memberList[1];
      otherUser = memberList[0];
    }

    room.displayName = `Private message with ${otherUser.name}`;

    let filteredMessages = [];
    if (user.blocked.includes(otherUser._id)) {
      filteredMessages = [
        {
          _id: 1,
          senderName: 'Admin',
          senderID: req.admin.id,
          contents: 'You have this user blocked.'
        }
      ];
    }
    else if (user.blockedBy.includes(otherUser._id)) {
      filteredMessages = [
        {
          _id: 1,
          senderName: 'Admin',
          senderID: req.admin.id,
          contents: 'This user has you blocked.'
        }
      ];
    }
    else {
      room.messages.forEach(function(message) {
        // Processs messages for displaying to user
        if (user.settings.safeMode) {
          // Word filter
          message.contents = message.contents.replace(/pepsi/gi, 'coke');
        }
        if (message.deleted === 1) {
          message.contents = '[deleted]';
        }

        let filteredMessage = {
          _id: message._id,
          contents: message.contents,
          senderID: message.sender._id,
          senderName: message.sender.name,
          contents: message.contents,
          timestamp: message.timestamp,
          toRoom: message.toRoom,
          timestampOffset: message.timestampOffset
        };

        filteredMessages.push(filteredMessage);
      });
    } 

    let data = {
      name: room.displayName,
      room_id: room._id,
      messages: filteredMessages
    };
    if(otherUser) {
      data.otherUser = {
        id: otherUser._id,
        name: otherUser.name
      }
    }

    return req.renderUI(res, 'privateRoom/view', data);
  });
}

exports.list = function(req, res, next) {
  PrivateRoom.find( {
    'members': {
      _id: req.session.user.id
    } 
  })
  .populate('members')
  .populate('pastMembers')
  .exec(function (err, privateRooms) {
    if(err) return next(err);
    if(!privateRooms) return next(req.buildError(404, 'Failed to fetch room list'));

    let rooms = [];
    for (let roomIndex in privateRooms) {
      let room = privateRooms[roomIndex];

      let memberList = [];
      if(room.members.length > 1) {
        memberList = room.members;
      }
      else {
        memberList = room.pastMembers;
      }

      room.displayName = memberList[0]._id.toString() === req.session.user.id.toString() ? memberList[1].name : memberList[0].name;
      rooms.push(
        {
          id: room._id,
          name: room.displayName
        }
      );
    };

    let data = {
      privateRooms: rooms
    };

    return req.renderUI(res, 'privateRoom/list', data);
  });
}

exports.leave = function (req, res, next) {
  if(!req.body.room_id) return next(req.buildError(400, 'Missing room ID'));
  
  PrivateRoom
  .findById(req.body.room_id)
  .exec(function (err, room) {
    if (err) return next(err);
    let redirect_url = '/privateRoom/list';
    if (!room) {
      req.flash('error', 'Room does not exist');
      return res.send({url: redirect_url});
    }
    if (room.members.indexOf(req.session.user.id) == -1) {
      req.flash('error', 'Not a member of this room');
      return res.send({url: redirect_url});
    }

    room.members.pull(req.session.user.id);

    room.save(function (err) {
      if(err) return next(err);

      let date = new Date();
      let newMessage = new Message({ 
        contents: `${req.session.user.name} has left the room`,
        sender: req.admin.id,
        toRoom: room._id,
        timezoneOffset: date.getTimezoneOffset()
      });

      room.addMessage(newMessage, function(err) {
        if (err) return next(err);
        return res.send({url: redirect_url});
      });
    });
  });
}

exports.delete = function (req, res, next) {
  return res.send('Unimplemented');
}
