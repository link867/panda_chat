let canRemoveMessage = function (user, room, message) {
  if (user.isAdmin) {
    // Admins can remove any message
    return true;
  }
  if (user.isGlobalModerator && !(message.sender.isAdmin || message.sender.isGlobalModerator)) {
    // Global mods can remove all messages but those sent by other global mods and admins
    return true;
  }
  if (room.moderators.indexOf(user._id) !== -1 && !(message.sender.isGlobalModerator || message.sender.isAdmin)) {
    // Room mods can remove messages except those sent by other room mods, global mods, or admins. Room owners can remove messages from room mods.
    if(room.owner.toString() === user._id.toString()) {
      return true;
    }
    if(room.moderators.indexOf(message.sender._id) > -1) {
      return false; 
    }
    return true;
  }

  return false;
};

let canUserBeBanned = function (banner, banee, room) {
  if (banee.isAdmin || banee.isGlobalModerator) {
    // Admins and global mods cannot be banned
    return false;
  }
  if (banner.isAdmin || banner.isGlobalModerator) {
    // Admins and global mods can ban everyone
    return true;
  }
  if(banee._id.toString() == room.owner && !(banner.isAdmin || banner.isGlobalModerator)) {
    // Room owner cannot be banned except by admins and global mods
    return false;
  }
  if (banner._id.toString() == room.owner || (room.moderators.indexOf(banner._id) > -1 && room.moderators.indexOf(banee._id) === -1)) {
    // Room mods cannot ban other room mods, except for room owner
    return true;
  }
  return false;
};

let canCloseOrDeleteRoom = function (user, room) {
  if (user.isAdmin || user.isGlobalModerator) {
    // Admins and Mods can close and delete
    return true;
  }
  if (user._id == room.owner) {
    // Room owners can close and delete their room
    return true;
  }
  return false;
};

module.exports = {
  canRemoveMessage,
  canUserBeBanned,
  canCloseOrDeleteRoom
}
