const User = require('../models/user.js');
const Message = require('../models/message.js');
const { DateTime } = require("luxon");

let iterateMessages = function (user) {
  //let now = DateTime.now().toISO;
  let hourAgo = DateTime.now().minus({ hours: 1 }).toISO();
  console.log(DateTime.now().minus({ hours: 1 }).toISO());
  Message
  .find()
  .populate('sender')
  .where('timeStamp').gte(hourAgo)
  .exec(function(err, messages) {
    if (err) console.log(err);
    console.log(JSON.stringify(messages));
  });
};

let updateThresholdFavor = function (user) {

};

let updateIntervalFavor = function (user) {
  
};

module.exports = {
  iterateMessages
};
