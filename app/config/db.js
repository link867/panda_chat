const mongoose = require('mongoose');
const config = require('./config');

const { db: { host, port, name } } = config;
const connectionString = `mongodb://${host}:${port}/${name}`;
const options = {
  bufferCommands: false,
  promiseLibrary: global.Promise,
  useNewUrlParser: true,
  useUnifiedTopology: true 
}

let ddb = mongoose.connection;
let mongoDB = mongoose.connect(connectionString, options).then( function() {
}).catch( function(err) {
  console.log(`Connection to ${config.db.name} failed! :\'\( Exiting app!` + '\n\n');
  process.exit(2);
});


ddb.on('connected', function () {
  console.log('Connected to database ' + config.db.name);
});

ddb.on('error', function (err, reject) {
  console.log(`DATABASE ERROR: [ ${err} ]`);
});
