let cron = require('node-cron');
let User = require('../../models/user.js');
let Message = require('../../models/message.js');

cron.schedule('* * * * *', () => {
  console.log('Running favor task');
  let now = new Date()
  now.setSeconds(0);
  let oneHourAgo = new Date()
  oneHourAgo.setHours(oneHourAgo.getHours() -1);
  oneHourAgo.setSeconds(0);
  console.log`[${now.toISOString()}][${oneHourAgo.toISOString()}]`;
  // Group by user so users who sent more than one message aren't duplicated
  Message.find({
    timeStamp: {
      $gte: oneHourAgo.toISOString(), 
      $lt: now.toISOString()
    }
  }, 
  'sender',
  {'group': 'sender'},  

  function (err, messages) {
    console.log(JSON.stringify(messages));
  });
});
