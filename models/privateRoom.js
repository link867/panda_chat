let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Room = require('./room.js');
let Message = require('./message.js');

let options = { discriminatorKey: 'kind' };

let privateRoomSchema = new Schema(
  {
    allowPictures: {
      type: Boolean,
      required: true
    },
    name: {
      required: false
    }
  },
  options
);

privateRoomSchema.methods.addMessage = function(message, callback) {
  let roomInstance = this;
  Message.createNewMessage(message, function(err, newMessage) {
    if (err) return callback(err);
    roomInstance.model('Room').update({ _id: roomInstance._id}, { $push: {messages: newMessage} }, function (err) {
      return callback(err);
    });
  });
};

let privateRoom = Room.discriminator('privateRoom', privateRoomSchema);
module.exports = privateRoom;
