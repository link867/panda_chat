let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let reportModel = new Schema({
  reporter: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  reportee: { 
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  timeStamp: {
    type: Date,
    default: Date.now
  },
  timezoneOffset: {
    type: Number
  },
  reason: {
    type: String,
  },
  message: {
    type: Schema.Types.ObjectId,
    ref: 'Message'
  }
});

let Report = mongoose.model('Report', reportModel);
module.exports = Report;
