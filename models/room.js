let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let options = { discriminatorKey: 'kind' };

let roomSchema = new Schema({
    name: {
      type: String,
      required: [true, 'Room name cannot be blank']
    },
    status: {
      type: Number,
      required: true,
      default: 1
    },
    creationDate: {
      type: Date,
      default: Date.now
    },
    members: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    pastMembers: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    banned: [{
      type: Schema.Types.ObjectId,
      ref: 'User',
    }],
    messages: [{
      type: Schema.Types.ObjectId,
      deleted: Boolean,
      ref: 'Message'
    }],
    favorMin: {
      type: Number,
      required: true,
      default: 0
    }
  },
  options
);
let Room = mongoose.model('Room', roomSchema);
module.exports = Room;
