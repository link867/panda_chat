let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let blockModel = new Schema({
  reporter: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  reportee: { 
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  timeStamp: {
    type: Date,
    default: Date.now
  },
  timezoneOffset: {
    type: Number,
    required: true
  },
  reason: {
    type: String,
  }
});

let Block = mongoose.model('Block', blockModel);
module.exports = Block;
