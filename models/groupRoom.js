let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Room = require('./room.js');

let options = { discriminatorKey: 'kind' };

let groupRoomSchema = new Schema(
  {
    owner: {
      type: Schema.Types.ObjectId, 
      ref: 'User',
      required: [true, 'Room must have an owner']
    },
    isPrivate: {
      type: Boolean,
      required: true
    },
    moderators: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }]
  },
  options
);

let groupRoom = Room.discriminator('groupRoom', groupRoomSchema);
module.exports = groupRoom;
