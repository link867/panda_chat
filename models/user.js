let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let userModel = new Schema({
  name: {
    type: String, 
    required: [true, 'Name cannot be blank']
  },
  email: {
    type: String,
    required: [true, 'Email cannot be blank']
  },
  password: {
    type: String,
    required: [true, 'Password cannot be blank']
  },
  passwordConf: {
    type: String,
    required: [true, 'Password confirmation cannot be blank']
  },
  age: {
    type: Number,
    required: true
  },
  isAdmin: {
    type: Boolean,
    required: true
  },
  isGlobalModerator: {
    type: Boolean,
    required: true
  },
  friends: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  friendedBy: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  blocked: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  blockedBy: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  settings: {
    safeMode: {
      type: Boolean,
      required: true,
      default: false
    },
    restrictPms: {
      type: Boolean,
      required: true,
      default: false
    },
    favorMin: {
      type: Number,
      required: true,
      default: 0
    },
  },
  favor: {
    type: Number,
    required: true,
    default: 0
  },
  lastBlocked: {
    type: Date
  },
  lastBanned: {
    type: Date
  },
  lastReported: {
    type: Date
  },
});

userModel.statics.authenticate = function (email, password, callback) {
  User.findOne({ email: email }, function (err, user) {
    if (err) return callback(err);
    if (!user) {
      return callback('User not found');
    }

    if (password === user.password) return callback(null, user);

    return callback('Incorrect password');
  });
}

userModel.statics.updateReputation = function(user, newReputation) {
  user.reputation += newReputation;
  user.save(function (err) {
    return;
  });
}

let User = mongoose.model('User', userModel);
module.exports = User;
