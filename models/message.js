let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let messageSchema = new Schema({
  contents: {
    type: String,
    required: [true, 'Message cannot be blank']
  },
  timeStamp: {
    type: Date,
    default: Date.now
  },
  timezoneOffset: {
    type: Number,
    required: true
  },
  sender: {
    type: Schema.Types.ObjectId, 
    ref: 'User',
    required: [true, 'Message must have a sender']
  },
  toRoom: {
    type: Schema.Types.ObjectId,
    ref: 'Room'
  },
  readStatus: {
    type: Boolean,
    required: true,
    default: false
  },
  deleted: {
    type: Number,
    required: true,
    default: 0
  },
  removed: {
    type: Number,
    required: true,
    default: 0
  }
});

messageSchema.statics.createNewMessage = function(message, callback) {
  let newMessage = new this(message);
  newMessage.save(function(err) {
    callback(err, newMessage);
  });
}

module.exports = mongoose.model('Message', messageSchema);
