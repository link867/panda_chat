const express = require('express');
const router = express.Router();
const messageController = require('../controllers/privateMessageController');
const util = require('util'); // FOR DEBUGGING

router.post('/send/:room_id', messageController.send);

router.get('/delete/:message_id', messageController.delete);

router.get('/remove/:message_id', messageController.remove);

module.exports = router;
