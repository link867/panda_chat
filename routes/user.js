const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

router.get('/create/', userController.createForm);

router.post('/create/', userController.create);

router.post('/login/', userController.login);

router.get('/logout/', userController.logout);

router.get('/profile/', userController.profileShow);

router.get('/profile/:user_id', userController.profileShow);

router.get('/update/', userController.profileUpdateForm);

router.post('/update/', userController.profileUpdate);

router.get('/password/change/', userController.passwordChangeForm);

router.post('/password/change/', userController.passwordChange);

router.get('/settings/', userController.settingsUpdateForm);

router.post('/settings/', userController.settingsUpdate);

router.get('/friend/add/:user_id', userController.friendAdd);

router.get('/friend/remove/:user_id/', userController.friendRemove);

router.get('/friend/list/', userController.friendList);

router.get('/delete/', userController.profileDelete);

router.get('/block/:blockee', userController.block);

router.get('/unblock/:blockee', userController.unBlock);

router.get('/viewBlocked/', userController.viewBlocked);

router.get('/report/message/:reportee_id/:room_id/:message_id', userController.reportMessageForm);

router.post('/report/message', userController.reportMessage);

module.exports = router;
