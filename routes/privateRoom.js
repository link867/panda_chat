const express = require('express');
const router = express.Router();
const controller = require('../controllers/privateRoomController');

router.post('/open/', controller.open);

router.get('/view/:room_id', controller.view);

router.get('/list/', controller.list);

router.post('/leave/', controller.leave);

router.post('/delete/', controller.delete);

module.exports = router;
