const express = require('express');
const router = express.Router();
const roomController = require('../controllers/roomController');

router.get('/view/:room_id', roomController.view);

router.get('/create/', roomController.createForm);

router.post('/create/', roomController.create);

router.get('/list/user', roomController.listUser);

router.get('/list/all', roomController.listAll);

router.post('/leave/', roomController.leave);

router.post('/join/', roomController.join);

router.get('/edit/:room_id', roomController.editForm);

router.post('/edit/', roomController.edit);

router.post('/delete/', roomController.delete);

router.post('/close/', roomController.close);

router.get('/mod/edit/:room_id', roomController.modEdit);

router.post('/mod/add/:room_id', roomController.modAdd);

router.post('/mod/remove/:room_id', roomController.modRemove);

router.get('/ban/:room_id/:banee_id', roomController.banUser);

router.get('/unban/:room_id/:banee_id', roomController.unBanUser);

router.get('/listBanned/:room_id', roomController.listBannedUsers);

module.exports = router;
