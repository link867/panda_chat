const express = require('express');
const router = express.Router();
const http = require('http');
const userController = require('../controllers/userController');

router.get('/', function (req, res, next) {
  if(req.session.user) {
    return req.renderUI(res, 'index', {user: req.session.user});
  } else {
    return req.renderUI(res, 'index', {});
  }
});

module.exports = router;
